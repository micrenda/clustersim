#include <cmath>
#include <ostream>

#include "FitterBase.hpp"
#include "RenderCsvProgress.hpp"
#include "SimulationBase.hpp"

using namespace std;
using namespace clustersim;

void RenderCsvProgress::initialize(SimulationBase& simulation)
{
	RenderCsvBase::initialize(simulation);
	csvfile << "time,time_log,clusters,volume,volume_transformed,volume_transformed_ratio,volume_transformed_ratio_log" << endl;
}


void RenderCsvProgress::execute(SimulationBase& simulation)
{
    RenderCsvBase::execute(simulation);

	csvfile << simulation.getCurrentTime()		<< ","
	<< simulation.getLogCurrentTime()			<< ","
	<< simulation.getClusterCount()       		<< ","
	<< simulation.getTotalVolume()        		<< ","
	<< simulation.getTransformedVolume()  		<< ","
	<< simulation.getTransformedRatio()   		<< ","
	<< simulation.getLogTransformedRatio()   	<< endl;
}


void RenderCsvProgress::finalize(SimulationBase& simulation)
{
	RenderCsvBase::finalize(simulation);
}


