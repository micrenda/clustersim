#include <vector>
#include <stdexcept>
#include "World.hpp"

using namespace std;
using namespace clustersim;

World::World(vector<float> sizes)
{

	this->dimensions = sizes.size();

	if (dimensions == 0)
		throw invalid_argument("The 'dimensions' parameter must be bigger than zero");
	
	if (dimensions != sizes.size())
		throw invalid_argument("The 'sizes' vector must have 'dimensions' elements");

	this->sizes = sizes;
	
	// Compute volume
	volume = 1;
	
	for (unsigned char d = 0; d < dimensions; d++)
		volume *= this->sizes[d];
	
		
	this->sizes = sizes;
}
