#include <cmath>
#include <algorithm>
#include <iostream>

#include "SimulationPixeled.hpp"
#include "World.hpp"
#include "Cluster.hpp"


using namespace clustersim;

SimulationPixeled::SimulationPixeled(World &w, float pixelSide) : SimulationBase(w), pixelSide(pixelSide)
{
    unsigned int spaceVolume = 1;
    pixelVolume = 1.;

    for (unsigned short d = 0; d < world.dimensions; d++)
    {
        spaceSizes.push_back(round(world.sizes[d] / pixelSide));
        spaceVolume *= spaceSizes[d];

        spaceScales.push_back(1);

        for (unsigned short e = 0; e < d; ++e)
            spaceScales[d] *= spaceSizes[e];

        pixelVolume *= pixelSide;
    }

    space = vector<DiscretizedPixel>(spaceVolume);
    space.assign(spaceVolume, DiscretizedPixel());

    spaceTransformed = 0;

    spaceAdjacents = calculateAdjacents(w);
}

SimulationPixeled::~SimulationPixeled()
{
    space.clear();
    space.shrink_to_fit();
}

float SimulationPixeled::getTransformedVolume()
{
    return world.volume * spaceTransformed / space.size();
}

float SimulationPixeled::getClusterVolume(Cluster &cluster)
{
    return spaceClusterVolumes[cluster.id];
}

Cluster *SimulationPixeled::clusterCreate(unsigned int id, float creationTime)
{
    return SimulationBase::clusterCreate(id, creationTime);
}

void SimulationPixeled::clusterPlacePre(Cluster &cluster)
{
    SimulationBase::clusterPlacePre(cluster);
}

unsigned long SimulationPixeled::pointToCoordinate(const vector<float> &point)
{
    unsigned int coordinate = 0;

    for (unsigned short d = 0; d < world.dimensions; d++)
        coordinate += (unsigned long)(point[d] / pixelSide) * spaceScales[d];

    return coordinate;
}

void SimulationPixeled::coordinateToPoint(unsigned long coordinate, std::vector<float> &point)
{
    for (short d = world.dimensions - 1; d >= 0; d--)
    {
        point[d] = (coordinate / spaceScales[d]) * pixelSide;
        coordinate %= spaceScales[d];
    }
}

void SimulationPixeled::clusterPlacePost(Cluster &cluster)
{
    SimulationBase::clusterPlacePost(cluster);
    markPixel(pointToCoordinate(cluster.center), cluster, configAllowInTransformed);
}

void SimulationPixeled::clusterGrowAllPre()
{
    SimulationBase::clusterGrowAllPre();
}

void SimulationPixeled::clusterGrow(Cluster &cluster)
{
    SimulationBase::clusterGrow(cluster);
}

void SimulationPixeled::clusterGrowAllPost()
{
    SimulationBase::clusterGrowAllPost();

    // We are growing them which a onion like algorithm
    bool complete = false;

    while (!complete)
    {
        bool found = false;

        for (Cluster* cluster: clusters)
        {
			vector<unsigned long> toMark;
			
            for (pair<const unsigned long, unsigned short> entry: spaceFreeAdjacents)
            {
                unsigned int coordinate = entry.first;

                if (space[coordinate].cluster == cluster)
                {
                    for (int adjacent: spaceAdjacents)
                    {
                        unsigned long coordinateAdjacent = periodicAdd(coordinate, adjacent);

                        if (space[coordinateAdjacent].cluster == nullptr || configAllowInTransformed)
                        {
                            vector<float> pointAdjacent(world.dimensions);
                            coordinateToPoint(coordinateAdjacent, pointAdjacent);

                            if (distance(cluster->center, pointAdjacent) <= cluster->radius)
                            {
                                toMark.push_back(coordinateAdjacent);
                                found |= true;
                            }
                        }
                    }
                }
            }
            
            found |= !toMark.empty();
            
            for (unsigned long coordinateAdjacent: toMark)
				markPixel(coordinateAdjacent, *cluster, configAllowInTransformed);
        }
		
		compactAdjacentMap();
        complete = !found;
    }
    
    
}

void SimulationPixeled::markPixel(const unsigned long coordinate, Cluster &cluster, const bool force)
{
    DiscretizedPixel &pixel = space[coordinate];

    bool wasExisting = pixel.cluster != nullptr;

    if (!wasExisting)
    {
        pixel.cluster = &cluster;
        spaceClusterVolumes[pixel.cluster->id]++;
        spaceTransformed++;

        updateAdjacentMap(coordinate);
    }
    else
    {
        if (force)
        {
            spaceClusterVolumes[pixel.cluster->id]--;
            pixel.cluster = &cluster;
            spaceClusterVolumes[pixel.cluster->id]++;
        }
    }


}

Cluster *SimulationPixeled::getClusterAtPoint(std::vector<float> &point)
{
    unsigned long coordinate = pointToCoordinate(point);
    return space[coordinate].cluster;
}



vector<int> SimulationPixeled::calculateAdjacents(const World &world)
{
    vector<short> base = {-1, 0, +1};
    unsigned int count = 1;

    for (unsigned short d = 0; d < world.dimensions; d++)
        count *= 3;

    vector<int> adjacents = vector<int>(count);

    for (unsigned int a = 0; a < count; a++)
    {
        adjacents[a] = 0;
        unsigned int divider = 1;

        for (unsigned short d = 0; d < world.dimensions; d++)
        {
            adjacents[a] += base[(a / divider) % 3] * spaceScales[d];
            divider *= 3;
        }
    }

	 // Removing the '0' adjacent because it is the point itself
	adjacents.erase(std::find(adjacents.begin(), adjacents.end(), 0));
	adjacents.shrink_to_fit();
	
    return adjacents;
}

unsigned long SimulationPixeled::periodicAdd(const unsigned long value1, const int value2)
{
    if (value2 >= 0)
        return (value1 + value2) % space.size();
    else if ((unsigned int)(-value2) > value1)
        return space.size() + value2 + value1;
    else
        return value1 + value2;
}

void SimulationPixeled::updateAdjacentMap(const unsigned long coordinate)
{
    unsigned short empty = 0;
    for (int adjacent: spaceAdjacents)
    {
            unsigned long nearCoordinate = periodicAdd(coordinate, adjacent);

            if (space[nearCoordinate].cluster != nullptr)
            {
                spaceFreeAdjacents[nearCoordinate]--;;
            }
            else
                empty++;
    }

    spaceFreeAdjacents[coordinate] = empty;
}

void SimulationPixeled::compactAdjacentMap()
{
	for (unordered_map<unsigned long, unsigned short>::iterator it = spaceFreeAdjacents.begin(); it != spaceFreeAdjacents.end(); )
	{
		if ((*it).second == 0)
			it = spaceFreeAdjacents.erase(it);
		else
			it++;
	}
}

unsigned int SimulationPixeled::getPreferredPixelSize(unsigned short axis)
{
    return spaceSizes[axis];
}
