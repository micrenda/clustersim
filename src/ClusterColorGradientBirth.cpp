#include <stdexcept>

#include "Cluster.hpp"
#include "ClusterColorGradientBase.hpp"
#include "ClusterColorGradientBirth.hpp"

namespace clustersim {
class SimulationBase;
}  // namespace clustersim

using namespace clustersim;

ClusterColorGradientBirth::ClusterColorGradientBirth(std::vector<unsigned int> colors, float maxTime): ClusterColorGradientBase(colors), maxTime(maxTime)
{
	if (maxTime <= 0)
		throw std::invalid_argument ("The value of 'maxTime' must be bigger than zero");
	
}

ClusterColorGradientBirth::~ClusterColorGradientBirth()
{
}


void ClusterColorGradientBirth::initialize(SimulationBase& simulation)
{
	valueStart = 0;
	valueEnd   = maxTime;
}
	

unsigned int ClusterColorGradientBirth::getColor(SimulationBase& simulation, Cluster& cluster)
{
	return getColorGradRgb(cluster.creationTime);
}

void ClusterColorGradientBirth::finalize(SimulationBase& simulation)
{
}

