#include <stdexcept>

#include "Cluster.hpp"
#include "ClusterColorGradientAge.hpp"
#include "SimulationBase.hpp"

using namespace clustersim;

ClusterColorGradientAge::ClusterColorGradientAge(std::vector<unsigned int> colors, float maxAge): ClusterColorGradientBase(colors), maxAge(maxAge)
{
	if (maxAge <= 0)
		throw std::invalid_argument ("The value of 'maxAge' must be bigger than zero");
	
}

ClusterColorGradientAge::~ClusterColorGradientAge()
{
}


void ClusterColorGradientAge::initialize(SimulationBase& simulation)
{
	valueStart = 0;
	valueEnd   = maxAge;
}
	

unsigned int ClusterColorGradientAge::getColor(SimulationBase& simulation, Cluster& cluster)
{
	float age = simulation.getCurrentTime() - cluster.creationTime;
	return getColorGradRgb(age);
}

void ClusterColorGradientAge::finalize(SimulationBase& simulation)
{
}

