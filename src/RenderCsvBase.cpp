#include <fstream>

#include "RenderCsvBase.hpp"
#include "SimulationBase.hpp"


using namespace std;
using namespace clustersim;

RenderCsvBase::RenderCsvBase(bool periodic, string basename): RenderBase(periodic), basename(basename)
{
}

void RenderCsvBase::initialize(SimulationBase& simulation)
{
	RenderBase::initialize(simulation);
	csvfile.open (buildFilename(simulation), std::fstream::in | std::fstream::out | std::fstream::app);
}

void RenderCsvBase::finalize(SimulationBase& simulation)
{

	csvfile.close();
	RenderBase::finalize(simulation);
}

string RenderCsvBase::buildFilename(const SimulationBase &simulation)
{
	string filename = basename + ".csv";

	if (simulation.getOutputDir() != "")
		filename = simulation.getOutputDir() + "/" + filename;

    return filename;
}
