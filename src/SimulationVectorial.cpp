#include <stdexcept>
#include <cmath>
#include <random>

#include "Cluster.hpp"
#include "SimulationVectorial.hpp"
#include "World.hpp"

using namespace clustersim;

SimulationVectorial::SimulationVectorial(World& w):  SimulationBase(w)
{

}

SimulationVectorial::~SimulationVectorial()
{
}

float SimulationVectorial::getTransformedVolume()
{
	if (!transformedVolumeValid)
	{
		unsigned int inside = 0;
		unsigned int outside = 0;
		
		generator.seed(std::random_device()());
		
		vector<uniform_real_distribution<float>> distributions;
		
		for (unsigned char d = 0; d < this->world.dimensions; d++)
			 distributions.push_back(uniform_real_distribution<float>(0, this->world.sizes[d]));

        //
        double dV = world.volume / configEstimateVolumePointCount;

        // Resetting cluster volumes
        transformedClusterVolumes.clear();
		
		for (unsigned int i = 0; i < configEstimateVolumePointCount; i++)
		{
			vector<float> point;
			
			for (unsigned char d = 0; d < this->world.dimensions; d++)
				point.push_back(distributions[d](generator));

            Cluster* cluster = getClusterAtPoint(point);

			if (cluster != nullptr)
            {
                transformedClusterVolumes[cluster->id] += dV;
                inside++;
            }
			else
				outside++;
		}
		
		transformedVolume = inside * dV;
		transformedVolumeValid = true;
	}
	
	return transformedVolume;
}
		
Cluster* SimulationVectorial::clusterCreate(unsigned int id, float creationTime)
{
	return SimulationBase::clusterCreate(id, creationTime);
}

void SimulationVectorial::clusterPlacePre(Cluster &cluster)
{
    SimulationBase::clusterPlacePre(cluster);
}

void SimulationVectorial::clusterPlacePost(Cluster &cluster)
{
    SimulationBase::clusterPlacePost(cluster);
}

void SimulationVectorial::clusterGrow(Cluster& cluster)
{
	SimulationBase::clusterGrow(cluster);
	transformedVolumeValid = false;
}


float SimulationVectorial::getClusterVolume(Cluster& cluster)
{
	return transformedClusterVolumes[cluster.id];
}

bool SimulationVectorial::isPointInCluster(Cluster& cluster, vector<float>& point)
{
	return distance(cluster.center, point) <= cluster.radius;
}

unsigned int SimulationVectorial::getPreferredPixelSize(unsigned short axis)
{
	float minSize = numeric_limits<float>::infinity();

	for (float size: world.sizes)
		if (size < minSize)
			minSize = size;

	float dsize = minSize / 800.;

    return world.sizes[axis] / dsize;
}


Cluster* SimulationVectorial::getClusterAtPoint (vector<float>& point)
{
    Cluster* currentCluster = nullptr;
    float    currentScore = -numeric_limits<float>::infinity();


    for (Cluster* cluster: clusters)
    {
        if (isPointInCluster(*cluster, point))
        {
            float score = getClusterScore(point, *cluster, selectionMethod);

            if (currentCluster == nullptr || score > currentScore)
            {
                currentCluster = cluster;
                currentScore   = score;
            }
        }
    }

    return currentCluster;
}



float SimulationVectorial::getClusterScore(vector<float>& point, Cluster& cluster, ClusterSelectionMethod selectionMethod)
{
    switch (selectionMethod)
    {
        case ClusterSelectionMethod::BIGGEST:
            return cluster.radius;

        case ClusterSelectionMethod::SMALLEST:
            return -cluster.radius;

        case ClusterSelectionMethod::OLDEST:
            return currentTime - cluster.creationTime;

        case ClusterSelectionMethod::NEWEST:
            return -(currentTime - cluster.creationTime);

        case ClusterSelectionMethod::NEAREST:
            return -distance(cluster.center, point);

        case ClusterSelectionMethod::FARTHEST:
            return distance(cluster.center, point);

        case ClusterSelectionMethod::SPEED:
            return (currentTime - cluster.creationTime) / distance(cluster.center, point);

        default:
            throw std::invalid_argument ("Internal error: unable to manage the provided selection method");

    }
}


