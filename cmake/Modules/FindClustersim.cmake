# Clustersim
# ---------
#
# Try to find Clustersim
#
# Once done this will define
#
# ::
#
#   CLUSTERSIM_FOUND 		- system has Clustersim
#   CLUSTERSIM_INCLUDE_DIR 	- the Clustersim include directory
#   CLUSTERSIM_LIBRARIES 	- Link these to use BZip2
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.



find_path(CLUSTERSIM_INCLUDE_DIR clustersim.hpp /opt/ PATH_SUFFIXES include)

if (NOT CLUSTERSIM_LIBRARIES)
    find_library(CLUSTERSIM_LIBRARIES NAMES clustersim PATH_SUFFIXES lib)
endif ()

# handle the QUIETLY and REQUIRED arguments and set CLUSTERSIM_FOUND to TRUE if
# all listed variables are TRUE

include(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CLUSTERSIM
                                  REQUIRED_VARS CLUSTERSIM_LIBRARIES CLUSTERSIM_INCLUDE_DIR)

mark_as_advanced(CLUSTERSIM_INCLUDE_DIR)
