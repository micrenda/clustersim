#pragma once


namespace clustersim
{
	class Cluster;
	class SimulationBase;

		  
	class ClusterColorBase
	{
		public:
		ClusterColorBase();
		virtual ~ClusterColorBase();
		
		virtual void initialize(SimulationBase& simulation) {};
		virtual unsigned int getColor(SimulationBase& simulation, Cluster& cluster) = 0; // Pure virtual
		virtual void finalize(SimulationBase& simulation)  {};
		
	};
}
