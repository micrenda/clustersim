#pragma once

#include "FitterBase.hpp"

namespace clustersim {
class SimulationBase;
}  // namespace clustersim

using namespace std;


namespace clustersim
{
	class FitterPolynomial: public FitterBase
	{
		public:
		FitterPolynomial();

        FitterPolynomial(float volumeRangeFrom, float volumeRangeTo);

        virtual void  finalize(SimulationBase& simulation);
		
		virtual float getFitCov00() { return (float)fitCov00; };
		virtual float getFitCov01() { return (float)fitCov01; };
		virtual float getFitCov11() { return (float)fitCov11; };
		virtual float getFitSumQ()  { return (float)fitSumQ;  };
		
		protected:
			double fitCov00 = 0, fitCov01 = 0, fitCov11 = 0, fitSumQ = 0;
	};
}
