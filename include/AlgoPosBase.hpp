#pragma once
#include "AlgoBase.hpp"

namespace clustersim
{

	class Cluster;
    class SimulationBase;

    class AlgoPosBase: public AlgoBase
	{
	public:
		virtual void place(SimulationBase& simulation, Cluster& cluster) = 0;
	};

}
