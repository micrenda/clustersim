#pragma once
#include <set>

#include "AlgoAddBase.hpp"

namespace clustersim {
    class SimulationBase;

	/**
	 * It will add a fix number of cluster at every given interval
	 */
	class AlgoAddIntervalled:  public AlgoAddBase
	{
		
	public:
		
		AlgoAddIntervalled(unsigned int quantity, float interval);
		
		int add(SimulationBase& simulation) override;
		
		float getNextExecution(SimulationBase& simulation) override;
		
		
		protected:
		
			unsigned int quantity;
			float		 interval;
			
		
	};

}
