#pragma once

#include <vector>
#include <unordered_map>

#include "DiscretizedPixel.hpp"
#include "SimulationBase.hpp"

namespace clustersim
{
    class Cluster;

    class World;
}  // namespace clustersim


namespace clustersim
{

    class SimulationPixeled : public SimulationBase
    {

    public:

        SimulationPixeled(World &w, float pixelSide);

        virtual ~SimulationPixeled();

        virtual float getTransformedVolume() override;
        virtual float getClusterVolume(Cluster& cluster) override;

        virtual unsigned int getPreferredPixelSize(unsigned short axis) override;
        virtual Cluster *getClusterAtPoint(std::vector<float> &point) override;


    protected:
        virtual Cluster* clusterCreate(unsigned int id, float creationTime) override;
        virtual void 	 clusterPlacePre (Cluster &cluster) override;
        virtual void     clusterPlacePost(Cluster &cluster) override;
        virtual void     clusterGrow(Cluster &cluster) override;

    protected:
        float pixelSide;
        float pixelVolume;

        std::vector<DiscretizedPixel> 		space;
        std::vector<unsigned int>     		spaceSizes;
        std::vector<unsigned int>     		spaceScales;
        std::vector<int>     				spaceAdjacents;
        unsigned int 				  		spaceTransformed = 0;
        //* This \p unordered_map contains the list of pixel which have free borders.
        std::unordered_map<unsigned long,unsigned short>	spaceFreeAdjacents;
        std::unordered_map<unsigned int, unsigned int>	    spaceClusterVolumes;



        /**
         * Mark the pixel \p i of \p space as belonging to \p cluster.
         *
         * @param coordinate Position in \p space
         * @param cluster Cluster to set at position \p i
         * @param force If true, the cluster will replace the existing one. If false, it will not replace it.
         * @return Return true is another cluster was existing at position \p i. False otherwise.
         */
        virtual void markPixel(const unsigned long coordinate, Cluster& cluster, const bool force);

        //unsigned long pixelToCoordinate(const vector<unsigned int> &point);
        unsigned long pointToCoordinate(const std::vector<float> &point);
		void          coordinateToPoint(const unsigned long coordinate, std::vector<float> &point);
        //void          coordinateToPixel(const unsigned long coordinate, vector<unsigned int> &point);

        std::vector<int> calculateAdjacents(const World &world);


        void updateAdjacentMap(const unsigned long coordinate);
        void compactAdjacentMap();

        virtual void clusterGrowAllPre() override;

        virtual void clusterGrowAllPost() override;


        unsigned long periodicAdd(const unsigned long value1, const int value2);
    };

}
