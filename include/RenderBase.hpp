#pragma once
#include <string>

#include "Schedulable.hpp"
#include "HasVolumeRange.hpp"


using namespace std;


namespace clustersim
{
    class SimulationBase;

	class RenderBase: public Schedulable, public HasVolumeRange
	{
		public:
		RenderBase(bool periodic);
		virtual ~RenderBase() = 0; // Pure virtual
		
		virtual void initialize(SimulationBase& simulation);
		virtual void execute(SimulationBase& simulation);
		virtual void finalize(SimulationBase& simulation);
	
		
		virtual float getNextExecution(SimulationBase& simulation) override;
		void executeProcess(string cmd);



		protected:

		bool periodic = false;
		float period = 0;



	};
}
