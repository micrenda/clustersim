#pragma once
namespace clustersim
{
	class HasVolumeRange
	{

	public:
		HasVolumeRange(float volumeRangeFrom, float volumeRangeTo);

		HasVolumeRange();

		void setVolumeRange(const float from, const float to);
		void clearVolumeRange();

        bool isHasVolumeRange() { return hasVolumeRange; };

        float getVolumeRangeFrom() { return volumeRangeFrom; }
        float getVolumeRangeTo()   { return volumeRangeTo; }


	protected:
		bool hasVolumeRange = false;

		float volumeRangeFrom = 0.;
		float volumeRangeTo   = 0.;

	};
}