#include <iostream>
#include <World.hpp>
#include <SimulationVectorial.hpp>
#include <AlgoAddSporadic.hpp>
#include <AlgoPosRandomUniform.hpp>
#include <AlgoGrowConstant.hpp>
#include <RenderCsvProgress.hpp>
#include <RenderCsvClusters.hpp>
#include <FitterPolynomial.hpp>
#include <RenderCsvFit.hpp>
#include <ClusterColorRandomAcid.hpp>
#include <RenderPngProgress.hpp>
#include <RenderCtiogaProgress.hpp>

using namespace std;
using namespace clustersim;


/**
 * This example perform the simulation of the nucleation proces of a 3D volume with
 * size of 10cm x 10cm x 10cm.
 * 
 * The nucleation process is a constant one ((are created 2 clusters every second).
 * 
 * All units are in SI format (if not speciefed differently)
 * 
 * 
 */
int main (int argc, char *argv[])
{
	// Creating the world
	World w = World({0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1}); // 10D world, 10cm x 10cm ... x 10cm
	
	SimulationVectorial s = SimulationVectorial(w);
	
	
	s.setAlgoAdd(new AlgoAddSporadic(50)); // Adding 50 cluster every second
	s.setAlgoPos(new AlgoPosRandomUniform());
	s.setAlgoGrow(new AlgoGrowConstant(0.005)); // Grow by 5 mm every second
	
	s.addFitter(new FitterPolynomial()); // Taking the fit every second
	
	s.addRender(new RenderCsvProgress("grow"));
	s.addRender(new RenderCsvClusters ("clusters"));
	s.addRender(new RenderCsvFit ("fit"));

	s.addRender(new RenderPngProgress("render", new ClusterColorRandomAcid()));

	s.addRender(new RenderCtiogaProgress("plot"));

	s.setOutputDir("ex5");
	
	s.start();

	for (float ratio = 0; ratio < 1.0; ratio += 0.05)
	{
		s.execute(ratio);
		cout << "Time : " << s.getCurrentTime()  <<" s, ratio: " << s.getTransformedRatio() * 100 << " %" <<  endl;
	}
	
	s.stop();
}
